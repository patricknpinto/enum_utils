# frozen_string_literal: true

module EnumsHelper
  def enum_for_select(model, attribute, selected = '', options = {})
    options_for_select(translated_enum(model, attribute, options), selected)
  end

  def translated_enum(model, attribute, options = {})
    attribute_name = options[:attribute_name] || attribute
    klass = model.to_s.camelize.constantize
    plural_name = attribute_name.to_s.pluralize

    klass.send(attribute.to_s.pluralize).keys.map do |key|
      [I18n.t("#{model}.#{plural_name}.#{key}", default: key.humanize), key]
    end
  end

  def yes_no_for_select
    [
      [I18n.t('globals.yes_no.true', default: 'Yes'), true],
      [I18n.t('globals.yes_no.false', default: 'No'), false]
    ]
  end

  def t_enum(record, attribute, default_value = '')
    model_name = record.class.name.underscore
    attribute_name = attribute.to_s.pluralize
    value = record.send(attribute)

    return default_value if value.blank?

    I18n.t("#{model_name}.#{attribute_name}.#{value}", default: value.humanize)
  end

  def t_boolean(value, key = nil, default_value = '')
    return default_value if value.nil?

    I18n.t("globals.#{key || 'yes_no'}.#{value}", default: value ? 'Yes' : 'No')
  end
end

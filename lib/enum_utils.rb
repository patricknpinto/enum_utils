# frozen_string_literal: true

require 'enum_utils/version'
require 'enum_utils/railtie'
require "#{File.expand_path('..', __dir__)}/app/helpers/enums_helper"

ActionView::Base.include(EnumsHelper)

module EnumUtils
  # Your code goes here...
end
